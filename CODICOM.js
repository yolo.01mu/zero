
//EXPRESION REGULAR PARA IMPRIMIR
const INICIO = /imprimir = '/g;
const FIANLIMPRE = /';/g; 
const SALIDAIMPRE=/^[imprimir = ']\w.*(';$)|(\s*)(';$)/gm;

//EXPRESION REGULAR PARA LA SUMA
const OPESUMA = /(\d*)[+].*(\d*)(;$)/;

//EXPRESION REGULAR PARA LA RESTA
const OPERESTA = /(\d*)[-].*(\d*)(;$)/;

//EXPRESION REGULAR PARA LA MULTIPLICACION
const OPEMULTIPLICACION = /(\d*)[*].*(\d*)(;$)/;

//EXPRESION REGULAR PARA LA DIVISION
const OPEDIVISION = /(\d*)[/].*(\d*)(;$)/;

//EXPRESION REGULAR PARA EL FOR
const INICIOCF = /desde = /g;
const INICIOCF2 = /hasta = /g;
const INICIOCF3 = /hacer = /g;

const SALIDAIMPRECF=/^[desde = ](\d*)[hasta = ](\d*)[hacer = ']\w.*(;$)|(\s*)(';$)/gs;

//EXPRESION REGULAR PARA LA DEFINICION DE NUMEROS
var DIGITO = /(\d*)/g;

//NUMERO DE CADENA
var NUC = /(\n)/m;

    function EXAMINACION(){
        const IMPRE = [];
        var AUX;
        RECIBIR = document.getElementById('ECODIGO').value;
        EVALUAR = RECIBIR.split(NUC);
        console.log(EVALUAR);

        for(let i = 0; i < EVALUAR.length; i++){
            if(EVALUAR[i].match(SALIDAIMPRE)){
            CADENA = EVALUAR[i].replaceAll(FIANLIMPRE, " ");
            IMPRE.push(CADENA.replaceAll(INICIO, " "));
            AUX = IMPRE.toString().replaceAll(",","</br>");

            }
            else{
                CONTADOR = 0;
                AUX = 'La sintaxis no es correcta';
            }

    //AQUI SE EJECUTA LA OPERACION DE SUMA
     if(OPESUMA.test(EVALUAR[i])){

        var RESULTADO = 0;
        x = EVALUAR[i].match(DIGITO);

        //FILTRA LAS CADENAS QUE TIENEN LA CARACTERISTICA DE DIGITO Y LO PASA A UN VALOR NUMERICO
        FILTRO = x.filter(Number).map(function(item) {
            return parseInt(item, 10);
        });
        //REALIZA LA OPERACION SUMA NO IMPORTANDO LA CANTIDAD DE DIGITOS INSERTADOS
        for(let i of FILTRO) RESULTADO+=i;
        CADENA = RESULTADO.toString();
        IMPRE.push(CADENA);
        AUX = IMPRE.toString().replaceAll(",","</br>");
    
    }

    //AQUI SE EJECUTA LA OPERACION DE RESTA 
    if(OPERESTA.test(EVALUAR[i])){

        var RESULTADO = 0;
        //FILTRA LAS CADENAS QUE TIENEN LA CARACTERISTICA DE DIGITO Y LO PASA A UN VALOR NUMERICO
        x = EVALUAR[i].match(DIGITO);
        FILTRO = x.filter(Number).map(function(item) {
            return parseInt(item, 10);
        });
         //REALIZA LA OPERACION RESTA CUANDO SOLO SE DETECTAN DOS DIGITOS
        if(FILTRO.length==2){
            RESULTADO = FILTRO[0]-FILTRO[1];
        }else{//REALIZA LA OPERACION RESTA NO IMPORTANDO LA CANTIDAD DE DIGITOS INSERTADOS
            for(let i = 1; i < FILTRO.length; i++){
                FILTRO[i]=FILTRO[i-1]-FILTRO[i];
                if(i==FILTRO.length)break;
            }
            RESULTADO = FILTRO[FILTRO.length-1];
        }
        CADENA = RESULTADO.toString();
        IMPRE.push(CADENA);
        AUX = IMPRE.toString().replaceAll(",","</br>");
    }

    //AQUI SE EJECUTA LA OPERACION DE MULTIPLICACION
    if(OPEMULTIPLICACION.test(EVALUAR[i])){
        var RESULTADO = 0;
        //FILTRA LAS CADENAS QUE TIENEN LA CARACTERISTICA DE DIGITO Y LO PASA A UN VALOR NUMERICO
        x = EVALUAR[i].match(DIGITO);
        FILTRO = x.filter(Number).map(function(item) {
            return parseInt(item, 10);
        });
        //REALIZA LA OPERACION MULTIPLICACION CUANDO SOLO SE DETECTAN DOS DIGITOS
        if(FILTRO.length==2){
            RESULTADO = FILTRO[0]*FILTRO[1];
        }else{//REALIZA LA OPERACION MULTIPLICACION NO IMPORTANDO LA CANTIDAD DE DIGITOS INSERTADOS
            for(let i = 1; i < FILTRO.length; i++){
                FILTRO[i] = FILTRO[i-1]*FILTRO[i];
                if(i==FILTRO.length)break;
            }
            RESULTADO = FILTRO[FILTRO.length-1];
        }
        CADENA = RESULTADO.toString();
        IMPRE.push(CADENA);
        AUX = IMPRE.toString().replaceAll(",","</br>");
    }

    //AQUI SE EJECUTA LA OPERACION DE DIVISION
    if(OPEDIVISION.test(EVALUAR[i])){
        var RESULTADO = 0;
         //FILTRA LAS CADENAS QUE TIENEN LA CARACTERISTICA DE DIGITO Y LO PASA A UN VALOR NUMERICO
         x = EVALUAR[i].match(DIGITO);
        FILTRO = x.filter(Number).map(function(item) {
            return parseInt(item, 10);
        });
        //REALIZA LA OPERACION DIVISION CUANDO SOLO SE DETECTAN DOS DIGITOS
        if(FILTRO.length==2){
            RESULTADO = FILTRO[0]/FILTRO[1];
        }else{//REALIZA LA OPERACION DIVISION NO IMPORTANDO LA CANTIDAD DE DIGITOS INSERTADOS
            for(let i = 1; i < FILTRO.length; i++){
                FILTRO[i]=FILTRO[i-1]/FILTRO[i];
                if(i==FILTRO.length)break;
            }
            RESULTADO = FILTRO[FILTRO.length-1];
        }
        CADENA = RESULTADO.toString();
        IMPRE.push(CADENA);
        AUX = IMPRE.toString().replaceAll(",","</br>");
    }

        }
        IMPRESION(AUX,CONTADOR);
        console.log(IMPRE); 
    }        

    
function IMPRESION(si, CONTADOR){
        document.getElementById('SCODIGO').innerHTML=si.replaceAll(FIANLIMPRE,"\n");
}
